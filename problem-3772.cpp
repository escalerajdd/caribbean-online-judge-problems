#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    int tests, aux;
    double sum;
    cin >> tests;
    for(int i=0; i<tests; i++) {
        sum = 0.0;
        for(int j=0; j<10; j++) {
            cin >> aux;
            sum += aux;
        }
        cout << fixed << setprecision(3) << sum * 0.85 << endl;
    }
}

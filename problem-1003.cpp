#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    int t, n, m, aux, max;
    cin >> t;
    for(int i=0; i<t; i++) {
        cin >> n >> m;
        int arr[n];
        for(int h=0; h<n; h++) {
            arr[h] = 0;
        }
        for (int j=0; j<m; j++) {
            for(int k=0; k<n; k++) {
                cin >> aux;
                arr[k] = arr[k] + aux;
            }
        }
        max = 0;
        for(int l=1; l<n; l++) {
            if(arr[l] > arr[max]) {
                max = l;
            }
        }
        cout << ++max << endl;
    }
}

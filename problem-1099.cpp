#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    bool x;
    long a, b, c;
    cin >> a;
    while(a != 0) {
        cin >> b >> c;
        x = false;
        if(a >= b && a >= c) {
            if(a*a == (b*b) + (c*c)) {
                x = true;
            }
        } else if(b >= c) {
            if(b*b == (a*a) + (c*c)) {
                x = true;
            }
        } else {
            if(c*c == (a*a) + (b*b)) {
                x = true;
            }
        }
        if(x)
            cout << "right" << endl;
        else
            cout << "wrong" << endl;
        cin >> a;
    }
}

#include <iostream>
#include <iomanip>

using namespace std;

int main() {
    double x, y, z = 0.00;
    cin >> x >> y;
    z = x * y;
    while(y - (z/x) < 1.00) {
        z--;
    }
    cout << fixed << setprecision(0) << ++z << endl;
}
